<?php

use PHPUnit\Framework\TestCase;

class MainTest extends TestCase
{
	public function testEmptyInput(): void
	{
		$this->expectException(EmptyStringException::class);

		isParenthesisValid();
	}

	/**
	 * @dataProvider parenthesesProvider
	 * @throws EmptyStringException
	 */
	public function testCorrectParentheses($openingParenthesis, $closingParenthesis): void
	{
		$this->assertTrue(
			isParenthesisValid(
				'Hello ' . $openingParenthesis . 'there' . $closingParenthesis
			)
		);
	}

	public function parenthesesProvider(): array
	{
		return [
			['', ''],
			['(', ')'],
			['[', ']'],
			['<', '>'],
		];
	}

	/**
	 * @dataProvider correctStringProvider
	 * @throws EmptyStringException
	 */
	public function testCorrectParenthesesCombination($string): void
	{
		$this->assertTrue(isParenthesisValid($string));
	}

	public function correctStringProvider(): array
	{
		return [
			['Hello (th[e]re)'],
			['<Hello (th[e]re)>'],
		];
	}

	/**
	 * @dataProvider incorrectStringProvider
	 * @throws EmptyStringException
	 */
	public function testIncorrectParenthesesCombination($string): void
	{
		$this->assertFalse(isParenthesisValid($string));
	}

	public function incorrectStringProvider(): array
	{
		return [
			['Hello (there'],
			['Hello there)'],
			['Hello )there('],
			['Hello (th[ere)'],
			['Hello (th[e)re]'],
			['Hello (th[ere)>'],
			['Hello (th[e<)re]>'],
		];
	}
}