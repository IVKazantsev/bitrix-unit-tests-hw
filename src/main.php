<?php

require_once __DIR__ . '/../boot.php';

/**
 * @param string $input
 *
 * @return bool
 * @throws EmptyStringException for empty input
 */
function isParenthesisValid(string $input = ''): bool
{
	$input = trim($input);
	if (empty($input))
	{
		throw new EmptyStringException('Empty input given');
	}

	$parenthesis = [];

	foreach (str_split($input) as $char)
	{
		switch ($char)
		{
			case '(':
				array_unshift($parenthesis, ')');
				break;
			case '[':
				array_unshift($parenthesis, ']');
				break;
			case '<':
				array_unshift($parenthesis, '>');
				break;
			case ')':
			case ']':
			case '>':
				if (empty($parenthesis) || $parenthesis[0] !== $char)
				{
					return false;
				}
				array_shift($parenthesis);
				break;
			default:
				break;
		}
	}

	if(!empty($parenthesis))
	{
		return false;
	}

	return true;
}